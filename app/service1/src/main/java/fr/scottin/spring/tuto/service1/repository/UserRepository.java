package fr.scottin.spring.tuto.service1.repository;

import fr.scottin.spring.tuto.service1.repository.model.User;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author scottin on 10/10/2017.
 */
public interface UserRepository extends JpaRepository<User, Integer> {
}
