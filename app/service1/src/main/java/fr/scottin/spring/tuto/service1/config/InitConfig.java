package fr.scottin.spring.tuto.service1.config;

import fr.scottin.spring.tuto.service1.repository.model.User;
import fr.scottin.spring.tuto.service1.repository.UserRepository;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Configuration;

/**
 * @author scottin on 10/10/2017.
 */
@Configuration
public class InitConfig implements CommandLineRunner {

  private UserRepository userRepository;

  public InitConfig(UserRepository userRepository){
    this.userRepository = userRepository;
  }

  @Override
  public void run(String... strings) throws Exception {
    userRepository.save(new User("Jean", "Dupont"));
  }
}
