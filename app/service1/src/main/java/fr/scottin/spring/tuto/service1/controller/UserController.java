package fr.scottin.spring.tuto.service1.controller;

import fr.scottin.spring.tuto.service1.repository.model.User;
import fr.scottin.spring.tuto.service1.repository.UserRepository;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @author scottin on 10/10/2017.
 */
@RestController
@RequestMapping("/users")
public class UserController {

  @Value("${server.port}")
  private String port;

  private UserRepository userRepository;

  public UserController(UserRepository userRepository){
    this.userRepository = userRepository;
  }

  @GetMapping
  public List<User> users(){
    return userRepository.findAll();
  }

  @GetMapping("/{id}")
  public User user(@PathVariable Integer id){
    return userRepository.findOne(id);
  }


  @GetMapping("/port")
  public String port(){
    return port;
  }
}
