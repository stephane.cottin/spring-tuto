package fr.scottin.spring.tuto.service3.controller;

import fr.scottin.spring.tuto.service3.controller.model.UserTest;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import java.util.Arrays;
import java.util.List;

/**
 * @author scottin on 10/10/2017.
 */
@RestController
@RequestMapping("/users-test")
public class TestController {

  private RestTemplate restTemplate;

  public TestController(RestTemplate restTemplate){
    this.restTemplate = restTemplate;
  }

  @GetMapping
  public List<UserTest> users(){
    return Arrays.asList(restTemplate.getForObject("http://service1/users", UserTest[].class));
  }

  @GetMapping("/{id}")
  public UserTest user(@PathVariable String id){
    return restTemplate.getForObject("http://service1/users/{id}", UserTest.class, id);
  }
}
