package fr.scottin.spring.tuto.service3.repository.model;

import org.springframework.data.jpa.domain.AbstractPersistable;

import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * @author scottin on 10/10/2017.
 */
@Entity
@Table(name = "companies")
public class Company extends AbstractPersistable<Integer> {

  private String name;

  private Integer pdgId;

  public Company() {
  }

  public Company(String name, Integer pdgId) {
    this.name = name;
    this.pdgId = pdgId;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public Integer getPdgId() {
    return pdgId;
  }

  public void setPdgId(Integer pdgId) {
    this.pdgId = pdgId;
  }
}
