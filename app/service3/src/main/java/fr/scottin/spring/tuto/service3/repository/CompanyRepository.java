package fr.scottin.spring.tuto.service3.repository;

import fr.scottin.spring.tuto.service3.repository.model.Company;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author scottin on 10/10/2017.
 */
public interface CompanyRepository extends JpaRepository<Company, Integer> {
}
