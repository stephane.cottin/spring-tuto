package fr.scottin.spring.tuto.service3.controller.model;

/**
 * @author scottin on 10/10/2017.
 */
public class UserTest {

  private String lastname;

  private String firstname;

  public String getLastname() {
    return lastname;
  }

  public void setLastname(String lastname) {
    this.lastname = lastname;
  }

  public String getFirstname() {
    return firstname;
  }

  public void setFirstname(String firstname) {
    this.firstname = firstname;
  }
}
