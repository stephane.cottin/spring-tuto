package fr.scottin.spring.tuto.service2.client.model;

import org.springframework.data.jpa.domain.AbstractPersistable;

import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * @author scottin on 10/10/2017.
 */
@Entity
@Table(name = "users")
public class User extends AbstractPersistable<Integer> {

  private String lastname;

  private String firstname;

  public User() {
  }

  public User(String lastname, String firstname) {
    this.lastname = lastname;
    this.firstname = firstname;
  }

  public String getLastname() {
    return lastname;
  }

  public void setLastname(String lastname) {
    this.lastname = lastname;
  }

  public String getFirstname() {
    return firstname;
  }

  public void setFirstname(String firstname) {
    this.firstname = firstname;
  }
}
