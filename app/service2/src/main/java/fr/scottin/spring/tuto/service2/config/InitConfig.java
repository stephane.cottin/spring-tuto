package fr.scottin.spring.tuto.service2.config;

import fr.scottin.spring.tuto.service2.repository.model.Company;
import fr.scottin.spring.tuto.service2.repository.CompanyRepository;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Configuration;

/**
 * @author scottin on 10/10/2017.
 */
@Configuration
public class InitConfig implements CommandLineRunner {

  private CompanyRepository companyRepository;

  public InitConfig(CompanyRepository companyRepository){
    this.companyRepository = companyRepository;
  }

  @Override
  public void run(String... strings) throws Exception {
    companyRepository.save(new Company("Google", 1));
  }
}
