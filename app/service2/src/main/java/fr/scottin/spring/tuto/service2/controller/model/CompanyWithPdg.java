package fr.scottin.spring.tuto.service2.controller.model;

import fr.scottin.spring.tuto.service2.client.model.User;

/**
 * @author scottin on 10/10/2017.
 */
public class CompanyWithPdg {

  private String name;

  private User pdg;

  public CompanyWithPdg(String name, User pdg) {
    this.name = name;
    this.pdg = pdg;
  }

  public User getPdg() {
    return pdg;
  }

  public void setPdg(User pdg) {
    this.pdg = pdg;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }
}
