package fr.scottin.spring.tuto.service2.controller;

import fr.scottin.spring.tuto.service2.client.Service1Client;
import fr.scottin.spring.tuto.service2.client.model.User;
import fr.scottin.spring.tuto.service2.controller.model.CompanyWithPdg;
import fr.scottin.spring.tuto.service2.repository.model.Company;
import fr.scottin.spring.tuto.service2.repository.CompanyRepository;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @author scottin on 10/10/2017.
 */
@RestController
@RequestMapping("/companies")
public class CompanyController {

  private CompanyRepository companyRepository;
  private Service1Client service1Client;

  public CompanyController(CompanyRepository companyRepository, Service1Client service1Client){
    this.companyRepository = companyRepository;
    this.service1Client = service1Client;
  }

  @GetMapping
  public List<Company> companies(){
    return companyRepository.findAll();
  }

  @GetMapping("/{id}")
  public CompanyWithPdg companyWithPdg(@PathVariable Integer id){
    Company company = companyRepository.getOne(id);

    User pdg = service1Client.getUserById(company.getPdgId());

    return new CompanyWithPdg(company.getName(), pdg);
  }

  @GetMapping("/fake/{id}")
  public CompanyWithPdg companyWithFakePdg(@PathVariable Integer id){
    Company company = companyRepository.getOne(id);

    User pdg = service1Client.getFakeUserById(company.getPdgId());

    return new CompanyWithPdg(company.getName(), pdg);
  }
}
