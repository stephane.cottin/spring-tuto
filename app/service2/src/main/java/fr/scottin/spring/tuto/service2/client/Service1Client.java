package fr.scottin.spring.tuto.service2.client;

import fr.scottin.spring.tuto.service2.client.model.User;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(name = "service1", fallback = Service1Client.Service1ClientFallback.class)
public interface Service1Client {

    @GetMapping("/users/{id}")
    User getUserById(@PathVariable(name = "id") Integer id);

    @GetMapping("/users/fake/{id}")
    User getFakeUserById(@PathVariable(name = "id") Integer id);

    @Component
    class Service1ClientFallback implements Service1Client{

        public User getUserById(@PathVariable(name = "id") Integer id) {
            return new User("fake1", "fake2");
        }

        public User getFakeUserById(@PathVariable(name = "id") Integer id) {
            return new User("fake1", "fake2");
        }
    }
}
