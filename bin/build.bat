CALL cd ..
CALL docker build -f bin/serviceregistry/Dockerfile --tag tuto/service-registry .
CALL docker build -f bin/proxy/Dockerfile --tag tuto/proxy .
CALL docker build -f bin/admin/Dockerfile --tag tuto/admin .
CALL docker build -f bin/service1/Dockerfile --tag tuto/service1 .
CALL docker build -f bin/service2/Dockerfile --tag tuto/service2 .
CALL docker build -f bin/service3/Dockerfile --tag tuto/service3 .