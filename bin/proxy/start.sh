#!/bin/sh
while ! nc -z service-registry 8761; do sleep 3; done
java $1 -Dspring.profiles.active=$2 -Djava.security.egd=file:/dev/./urandom -jar /proxy.jar